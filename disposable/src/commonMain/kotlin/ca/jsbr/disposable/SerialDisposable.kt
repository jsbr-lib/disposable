package ca.jsbr.disposable

/**
 * A Disposable container that allows atomically updating/replacing the contained
 * Disposable with another Disposable, disposing the old one when updating plus
 * handling the disposition when the container itself is disposed.
 */
class SerialDisposable(init: Disposable = Disposable.empty) : Disposable {
    var isDisposed = false
        private set

    var disposable: Disposable = init
        set(value) {
            if (isDisposed)
                value.dispose()
            val prev = field
            field = value
            prev.dispose()
        }

    override fun dispose() {
        if (!isDisposed)
            disposable.dispose()
        isDisposed = true
    }
}