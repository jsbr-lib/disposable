package ca.jsbr.disposable

import kotlin.test.Test
import kotlin.test.assertEquals

class CompositeDisposableTest {
    @Test
    fun shouldBeAbleToDisposeAllContainedDisposable() {
        var counter = 0
        val compositeDisposable = CompositeDisposable(arrayListOf(Disposable.create { counter++ }))
        compositeDisposable.add { counter++ }
        compositeDisposable.add(Disposable.create { counter++ })
        assertEquals(counter, 0)
        compositeDisposable.dispose()
        assertEquals(3, counter)
        compositeDisposable.dispose()
        assertEquals(counter, 3)
    }

    @Test
    fun shouldAutomaticallyDisposeDisposableIfContainerIsAlreadyDisposed() {
        var counter = 0
        val compositeDisposable = CompositeDisposable()
        compositeDisposable.dispose()
        compositeDisposable.add { counter++ }
        assertEquals(counter, 1)
    }
}