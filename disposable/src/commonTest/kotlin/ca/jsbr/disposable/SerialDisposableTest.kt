package ca.jsbr.disposable

import kotlin.test.Test
import kotlin.test.assertTrue

class SerialDisposableTest {

    @Test
    fun shouldDisposeTheContainedDisposableWhenSerialDisposableIsDisposed() {
        var called = false
        val fixture = SerialDisposable(ActionDisposable { called = !called })
        fixture.dispose()
        assertTrue(called)
    }

    @Test
    fun shouldDisposeThePreviousDisposableWhenIsReplaced() {
        var called = false
        var called2 = false
        val fixture = SerialDisposable(ActionDisposable { called = !called })
        fixture.disposable = ActionDisposable { called2 = !called2 }
        assertTrue(called)
        assertTrue(!called2)
    }

    @Test
    fun shouldDisposeNewDisposableIfSerialDisposableIsAlreadyDisposed() {
        var called = false
        val fixture = SerialDisposable()
        fixture.dispose()
        fixture.disposable = ActionDisposable { called = !called }
        assertTrue(called)
    }
}