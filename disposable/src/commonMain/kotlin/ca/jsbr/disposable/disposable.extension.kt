package ca.jsbr.disposable


fun Disposable.addTo(compositeDisposable: CompositeDisposable) = compositeDisposable.add(this)